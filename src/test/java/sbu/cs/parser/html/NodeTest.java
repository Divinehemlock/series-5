package sbu.cs.parser.html;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class NodeTest {

    private static final String sampleHTML1 = "<html>\n" +
            "<body>\n" +
            "\n" +
            "<p>This text is normal.</p>\n" +
            "\n" +
            "<p><b>This text is bold.</b></p>\n" +
            "\n" +
            "</body>\n" +
            "</html>";
    private static final String sampleHTML2 = "<html>\n" +
            "<body>\n" +
            "\n" +
            "<h2>Width and Height Attributes</h2>\n" +
            "\n" +
            "<p>The width and height attributes of the img tag, defines the width and height of the image:</p>\n" +
            "\n" +
            "</img src=\"img_girl.jpg\" width=\"500\" height=\"600\">\n" +
            "\n" +
            "</body>\n" +
            "</html>";
    private static final String sampleHTML3 = "<html>\n" +
            "<body>\n" +
            "\n" +
            "<h1>The input formaction attribute</h1>\n" +
            "\n" +
            "<p>The formaction attribute specifies the URL of a file that will process the input when the form is submitted.</p>\n" +
            "\n" +
            "<form action=\"/action_page.php\">\n" +
            "  <label for=\"fname\">First name:</label>\n" +
            "  </input type=\"text\" id=\"fname\" name=\"fname\"></br></br>\n" +
            "  <label for=\"lname\">Last name:</label>\n" +
            "  </input type=\"text\" id=\"lname\" name=\"lname\"></br></br>\n" +
            "  </input type=\"submit\" value=\"Submit\">\n" +
            "  </input type=\"submit\" formaction=\"/action_page2.php\" value=\"Submit_as_Admin\">\n" +
            "</form>\n" +
            "\n" +
            "</body>\n" +
            "</html>";

    private static final String sampleHTML4 = "<html>\n" +
            "<head>\n" +
            "<title>This is a Site!</title>\n" +
            "</head>\n" +
            "<body>\n" +
            "<p>This is a paragraph!</p>\n" +
            "</body>\n" +
            "</html>";

    @Test
    void getStringValue1() {
        Node parse = HTMLParser.parse(sampleHTML1);
        Node node = parse.getChild(0).getChild(1);
        assertEquals("<b>This text is bold.</b>", node.getStringInside());
        assertEquals("This text is bold.", node.getChild(0).getStringInside());
    }

    @Test
    void getStringValue2() {
        Node parse = HTMLParser.parse(sampleHTML2);
        String stringInside = parse.getChild(0).getChild(2).getStringInside();
        assertNull(stringInside);
        stringInside = parse.getChild(0).getChild(0).getStringInside();
        assertEquals("Width and Height Attributes", stringInside);
    }

    @Test
    void getStringValue3(){
        Node parse = HTMLParser.parse(sampleHTML4);
        String stringInside = parse.getChild(0).getChild(0).getStringInside();
        assertEquals("This is a Site!", stringInside);
        Node bodyNode = parse.getChild(1);
        stringInside = bodyNode.getChild(0).getStringInside();
        assertEquals("This is a paragraph!", stringInside);
    }

    @Test
    void getAttributeValue2() {
        Node parse = HTMLParser.parse(sampleHTML2);
        Node node = parse.getChild(0).getChild(2);
        assertEquals("img_girl.jpg", node.getAttributeValue("src"));
        assertEquals("500", node.getAttributeValue("width"));
        assertEquals("600", node.getAttributeValue("height"));
    }

    @Test
    void getAttributeValue3() {
        Node parse = HTMLParser.parse(sampleHTML3);
        Node node = parse.getChild(0).getChild(2);
        assertEquals("/action_page.php", node.getAttributeValue("action"));
        assertEquals("fname", node.getChild(0).getAttributeValue("for"));
        assertEquals("text", node.getChild(1).getAttributeValue("type"));
        assertEquals("fname", node.getChild(1).getAttributeValue("name"));
        assertEquals("lname", node.getChild(4).getAttributeValue("for"));
        assertEquals("submit", node.getChild(9).getAttributeValue("type"));
        assertEquals("/action_page2.php", node.getChild(9).getAttributeValue("formaction"));
        assertEquals("Submit as Admin", node.getChild(9).getAttributeValue("value"));
    }
}
